" General settings
" ----------------
set nocompatible
set autoindent
set smartindent			" Indenting (cindent or smartindent)
set linebreak           " Don't break words
set ruler               " Show the cursor position all the time
set spelllang=en_us     " Set the default language to US-English
syntax on
set pastetoggle=<F2>	" set INSERT in paste mode


" Searching & replacing
" ---------------------
set incsearch		" Incremental search


" Tab behavior
" ------------
set tabstop=4			" Size of a tab
set shiftwidth=4        " Number of spaces inserted for indentation
set showmatch
set guioptions-=T
set vb t_vb=
set nohls
set virtualedit=all
set bs=2

filetype plugin on 
let g:pydiction_location ='.vim/complete-dict'

